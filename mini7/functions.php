<?php
function getPosts($return_assoc = 0)
{
    $posts  = json_decode(file_get_contents(POST_DB), $return_assoc);
    return array_reverse((array) $posts);
}

function getUsers($return_assoc = 0)
{
    $users  = json_decode(file_get_contents(USERS_DB), $return_assoc);
    return array_reverse((array) $users);
}

// total_items = 100
// page_size = 10
// page = 3;

// start = ( page-1) * page_size
// end = start + page_size

function getPagePosts($posts, $page = 1, &$num_pages)
{
    $posts_arr = (array) $posts;
    $num_pages = ceil(sizeof($posts_arr) / POST_PER_PAGE);
    $start = ($page - 1) * POST_PER_PAGE;
    return (object) array_slice($posts_arr, $start, POST_PER_PAGE);
}

function getCatPosts($cat_slug)
{
    $posts  = getPosts();
    $cat_posts = array();
    foreach ($posts as $post) {
        if ($post->cat != $cat_slug) {
            continue;
        }
        $cat_posts[] = $post;
    }
    return $cat_posts;
}

function getCatItem($cat_slug)  // python
{
    $cats  = getCats();
    foreach ($cats as $slug => $cat) {
        if ($slug == $cat_slug) {
            return (object) $cat;
        }
    }
    return null;
}

function getCats($return_assoc = 0)
{
    $cats  = json_decode(file_get_contents(CATEGORIES_DB), $return_assoc);
    return array_reverse((array) $cats);
}

function isSearch()
{
    return isset($_GET['s']) && !empty($_GET['s']);
}
function isCat()
{
    return isset($_GET['cat']) && !empty($_GET['cat']);
}

function searchPosts($keyword)
{
    $posts  = getPosts();
    $finded_posts = array();
    foreach ($posts as $post) {
        if ((strpos($post->content, $keyword) !== false) || (strpos($post->title, $keyword) !== false)) {
            $post->title = str_replace($keyword, "<mark style='color:red'>$keyword</mark>", $post->title);
            $post->content = str_replace($keyword, "<mark style='color:red'>$keyword</mark>", $post->content);
            $finded_posts[] = $post;
        }
    }

    return $finded_posts;
}

// $post->{title,author,content}
function savePost(stdClass $post): bool
{
    $posts  = getPosts(1);
    $posts[] = (array) $post;
    $posts_json = json_encode($posts);
    file_put_contents(POST_DB, $posts_json);
    return true;
}


function isAdmin($user_name = 0)
{
    return 1 and isLoggedIn();
}

function isLoggedIn()
{
    return isset($_SESSION['login']);
}

function login($email, $password)
{
    $users = getUsers();
    // var_dump($users);
    foreach ($users as $user) {
        if ($user->password == $password && $user->email == $email) {
            $_SESSION['login'] = $user->email;
            return true;
        }
    }
    return false;
}

function logout()
{
    session_destroy();
    jsRedirect($_SERVER['HTTP_REFERER'], 1);
}

function siteUrl($uri = '')
{
    return BASE_URL . $uri;
}


function panelUrl($uri = '')
{
    return BASE_URL . 'panel/' . $uri;
}


function jsRedirect($url, $secs)
{
    echo "
    <script>
         setTimeout(function(){
            window.location.href = '" . $url . "';
         }, " . $secs * 1000 . ");
      </script>";
}
