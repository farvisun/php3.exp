<?php
include_once "Classes/Button.class.php";

$btn1 = new Button("Click");  /// instantiation
$btn1->addStyle('color:red');
$btn1->addStyle('border-radius:5px');
$btn1->addStyle('border:6px solid green');
$btn1->render();

$btn2 = new Button("Click There");  /// instantiation
$btn2->addStyle('color:blue');
$btn2->render();

$btn4 = new Button("Click There");  /// instantiation
$btn4->addStyle('width:200px');
$btn4->addStyle('height:200px');
$btn4->render();
