<?php

class Button
{
    public $title;
    public $style;

    /**
     * Class constructor.
     */
    public function __construct($title, $style = '')
    {
        $this->title = $title;
        $this->style = $style;
    }

    public function render()
    {
        echo "<button style='{$this->style}'>{$this->title}</button> ";
    }

    public function addStyle($cssStyle)
    {
        $this->style .= $cssStyle . ';';
    }

    public function generateRandomButtons($count)
    {
        if ($count > 0 && is_numeric($count)) {
            for ($i = 1; $i <= $count; $i++) {
                $btn1 = new Button("Button $i");  /// instantiation
                $btn1->addStyle("font-size: " . $i * 5);
                $btn1->addStyle("color: rgb(" . rand(0, 255) . "," . rand(0, 255) . "," . rand(0, 255) . ")");
                $btn1->render();
            }
        }
    }
}
