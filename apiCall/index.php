<?php
include "vendor/autoload.php";

$client = new \GuzzleHttp\Client();
$response = $client->request('GET', 'http://filemarket.exp/api/v1/files');

echo $response->getStatusCode(); # 200
echo $response->getHeaderLine('content-type'); # 'application/json; charset=utf8'
echo $response->getBody(); # '{"id": 1420053, "name": "guzzle", ...}'