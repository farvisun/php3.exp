<?php

namespace App;

class Product
{
    private $productID;
    /**
     * Product title
     *
     * @var string
     */
    private $title;
    /**
     * Product price
     *
     * @var integer
     */
    private $price;

    public function __construct(?string $title, ?int $price)
    {
        $this->title = $title;
        $this->price = $price;
    }
    /**
     * Get product title
     *
     * @return string
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }
    /**
     * Get product price
     *
     * @return integer
     */
    public function getPrice(): ?int
    {
        return $this->price;
    }
}
