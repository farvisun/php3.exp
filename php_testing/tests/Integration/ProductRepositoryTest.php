<?php

namespace Tests\Integration;

use App\Order;
use App\Product;
use App\Repositories\ProductRepository;
use PDO;
use PHPUnit\Framework\TestCase;

class ProductRepositoryTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();
    }
    public function tearDown()
    {
        parent::tearDown();
    }
    /**
     * it can make a new product and save in database
     * @test
     *
     * @return void
     */
    public function it_can_create_a_product()
    {
        $db = new PDO('mysql:host=localhost;dbname=shop', 'root', '');
        $productRepository = new ProductRepository($db);
        $product = new Product("first product", 150000);
        $creationResult = $productRepository->create($product);
        $this->assertTrue($creationResult);
    }
    /**
     * @test
     *
     * @return void
     */
    public function it_must_thorw_an_exception_id_invalid_data_is_passed()
    {
        $this->expectException(\Exception::class);
        $db = new PDO('mysql:host=localhost;dbname=shop', 'root', '');
        $productRepository = new ProductRepository($db);
        $product = new Product(null, null);
        $creationResult = $productRepository->create($product);
        $this->assertTrue($creationResult);
    }
}
