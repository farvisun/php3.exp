<?php

namespace Tests\Unit;

use App\Order;
use App\Product;
use PHPUnit\Framework\TestCase;

class OrderTest extends TestCase
{
    /**
     * @test
     *
     * @return void
     */
    public function it_can_make_a_instance_from_class()
    {
        $order = new Order();
        $this->assertInstanceOf(Order::class, $order);
    }
    /**
     * @test
     *
     * @return void
     */
    public function an_order_must_have_add_product_method()
    {
        $order = new Order();
        $this->assertTrue(method_exists($order, 'addProduct'));
    }
    /**
     * @test
     *
     * @return void
     */
    public function an_order_must_have_many_products()
    {
        $order = new Order();
        $order->addProduct(new Product("product 1", 100000));
        $order->addProduct(new Product("product 2", 150000));
        $order->addProduct(new Product("product 3", 300000));
        $this->assertEquals(count($order->getProducts()), 3);
    }
    /**
     * @test
     *
     * @return void
     */
    public function an_order_must_have_a_calculate_total_price()
    {
        $order = new Order;
        $this->assertTrue(method_exists($order, 'calculateTotalPrice'));
    }
    /**
     * @test
     *
     * @return void
     */
    public function it_can_calculate_total_price()
    {
        $order = new Order();
        $order->addProduct(new Product("product 1", 100000));
        $order->addProduct(new Product("product 2", 150000));
        $order->addProduct(new Product("product 3", 300000));
        $order->calculateTotalPrice();
        $this->assertEquals($order->getTotalPrice(), 550000);
    }
}
