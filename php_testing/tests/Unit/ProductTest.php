<?php

namespace Tests\Unit;

use App\Product;
use PHPUnit\Framework\TestCase;

class ProductTest extends TestCase
{
    /**
     * @test
     *
     * @return void
     */
    public function it_can_make_a_instance_from_class()
    {
        $title = "new product";
        $price = 150000;
        $product = $this->makeProduct($title, $price);
        $this->assertInstanceOf(Product::class, $product);
    }
    /**
     * @test
     *
     * @return void
     */
    public function a_product_must_have_a_title()
    {
        $title = "new product";
        $price = 150000;
        $product = $this->makeProduct($title, $price);
        $this->assertEquals($product->getTitle(), $title);
    }
    /**
     * @test
     *
     * @return void
     */
    public function a_product_must_have_a_price()
    {
        $title = "new product";
        $price = 150000;
        $product = $this->makeProduct($title, $price);
        $this->assertEquals($product->getPrice(), $price);
    }

    private function makeProduct($title, $price)
    {
        $product = new Product($title, $price);
        return $product;
    }
}
