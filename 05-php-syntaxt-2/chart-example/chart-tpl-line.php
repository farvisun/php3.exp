<div class="dollorChart"></div>
<link rel="stylesheet" href="//cdn.jsdelivr.net/chartist.js/latest/chartist.min.css">
<script src="//cdn.jsdelivr.net/chartist.js/latest/chartist.min.js"></script>
<script>
    new Chartist.Line('.dollorChart', {
        labels: <?= $daysStr ?>,
        series: [
            <?= $usdPricesStr ?>
        ]
    }, {
        low: 0,
        showArea: true
    });
</script>