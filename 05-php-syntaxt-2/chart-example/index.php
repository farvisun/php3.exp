<a href="?chart-type=pie">Pie</a>
<a href="?chart-type=line">Line</a>
<?php

$days = [1, 2, 3, 4, 5, 6, 7];
$usdPrices = [3300, 3690, 8000, 9000, 12000, 15000, 29000];

$daysStr = json_encode($days);
$usdPricesStr = json_encode($usdPrices);


if ($_GET['chart-type'] == 'pie') {
    include "chart-tpl-pie.php";
} else if ($_GET['chart-type'] == 'line') {
    include "chart-tpl-line.php";
} else {
    echo "No chart !";
}
?>