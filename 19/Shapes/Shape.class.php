<?php

interface IShape
{
    public function area();
    public function surface();
}

final class Shape implements IShape
{
    private $id;
    private static $shape_count = 0;
    const PI = 3.14;
    /**
     * Class constructor.
     */
    public function __construct()
    {
        $this->id = rand(10, 99);
        echo "Shape($this->id) Created! <br>";
        // HW: Late Static Binding
        self::$shape_count++;
    }


    // public static function get_shape_count(); // getter
    public static function count()
    {
        return self::$shape_count;
    }

    /*    
    // setter
    public static function set_shape_count($new_count)
    {
        self::$shape_count = $new_count;
    }
*/

    public function set_id($id)
    {
        // if (Auth::can_set_id()) {
        $this->id = $id;
        // } else {
        //     echo "Permision Denied!";
        // }
    }

    public final function name()
    {
        return self::class;
    }

    /**
     * Class destructor.
     */
    public function __destruct()
    {
        echo "Shape($this->id) Destructed! <br>";
    }
}

// abstract class X extends Shape
// { }

class Circle extends Shape
{

    private $r; // radius

    public function __construct(int $radius)
    {
        $this->r = $radius;
    }

    public function area()
    {
        return M_PI * $this->r * 2;
    }
    public function surface()
    {
        return M_PI * $this->r * $this->r;
    }
}

class Rectangle extends Shape
{
    private $w; // Width
    private $h; // Height

    public function __construct(int $w, int $h)
    {
        $this->h = $h;
        $this->w = $w;
    }
    public function area()
    {
        return 2 * ($this->h + $this->w);
    }
    public function surface()
    {
        return $this->h * $this->w;
    }
}


class Square extends Shape
{
    public function area()
    { }
    public function surface()
    { }
}
