<?php
function p($var)
{
    echo "<div>$var</div>";
}
/*
function increase(&$var)
{
    return ++$var;
}

$a = 5;
increase($a);
increase($a);
increase($a);
echo $a;



function changeName($userObj, $new_name)
{
    $userObj->name = $new_name;
}
$user = new stdClass;
$user->name = "Ali";
changeName($user, "Hassan");
var_dump($user);

*/

function arrSum($arr)
{
    $arr[5] = 1111;
    $arr[] = 2222;
    $a = 11;
    $sum = array_sum($arr);
    return $sum;
}
function arrSumRef(&$arr)
{
    $arr[5] = 1111;
    $arr[] = 2222;
    $a = 11;
    $sum = array_sum($arr);
    return $sum;
}

// $a = 5;
$arr = array_fill(0, 1000000, 7);

$m1 = memory_get_usage();
arrSum($arr);
arrSum($arr);
arrSum($arr);
arrSum($arr);
$m2 = memory_get_usage();
p($m2 - $m1);

$m1 = memory_get_usage();
arrSumRef($arr);
arrSumRef($arr);
arrSumRef($arr);
arrSumRef($arr);
$m2 = memory_get_usage();
p($m2 - $m1);



// $ar2 = [22, 5, 33, 6, 7, 222, 5, 7, 8];
// sort($ar2);
// var_dump($ar2);
