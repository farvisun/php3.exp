<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);


function func_name($param1, $param2)
{
    // function code
    return 1;
}


function sum(int $a, int $b): float
{
    return $a + $b + 1.51;
}

function sum2($a, $b, $c = 0, $d = 0)
{
    return $a + $b + $c + $d;
}

function sum3()
{
    $numbers = func_get_args();
    return array_sum($numbers);
}

// same as sum3
function sum4(...$args)
{
    return array_sum($args);
}
